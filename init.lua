-- CONCRETE
minetest.register_alias("infrastructure:concrete","basic_materials:concrete_block")
minetest.register_alias("infrastructure:stair_concrete","streets:stair_sidewalk_alt")
minetest.register_alias("infrastructure:fence_concrete","technic:concrete_post")

-- PREFAB
minetest.register_alias("prefab:concrete_fence","technic:concrete_post")

-- SEAPLANTS
minetest.register_alias("seaplants:kelpgreen","air")
minetest.register_alias("seaplants:kelpgreenmiddle","air")
minetest.register_alias("seaplants:kelpbrown","air")
minetest.register_alias("seaplants:kelpbrownmiddle","air")
minetest.register_alias("seaplants:seagrassgreen","air")
minetest.register_alias("seaplants:seagrassred","air")
minetest.register_alias("seaplants:seaplantssandkelpgreen","air")
minetest.register_alias("seaplants:seaplantsdirtkelpgreen","air")
minetest.register_alias("seaplants:seaplantssandkelpbrown","air")
minetest.register_alias("seaplants:seaplantsdirtkelpbrown","air")
minetest.register_alias("seaplants:seaplantssandseagrassgreen","air")
minetest.register_alias("seaplants:seaplantsdirtseagrassgreen","air")
minetest.register_alias("seaplants:seaplantssandseagrassred","air")
minetest.register_alias("seaplants:seaplantsdirtseagrassred","air")

-- STAIRS 
minetest.register_alias("stairs:slab_woodglass","building_blocks:woodglass")
minetest.register_alias("stairs:slab_seastonebrick_cyan","seastonebrick:slab_seastonebrick_cyan")

-- MISC
--minetest.register_alias("vacuum:vacuum","air")
